import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-redirect',
  templateUrl: './redirect.component.html',
  styleUrls: ['./redirect.component.css']
})
export class RedirectComponent implements OnInit {

  constructor(
    private authservice: AuthService,
    private route: ActivatedRoute,
    private router: Router

    ) { }

  ngOnInit() {
    /* On s'inscrit a l'observable qui distribue les  paramètres de la route */
    this.route.queryParamMap.subscribe((params) => {
   /*
   récupère le mode
   */
   const  mode =  params.get('mode');
   /*
   si mode vérification email
   */
   if (mode === 'verifyEmail') {
    this.authservice.validateEmail(params.get('oobCode')).then(() => {
      console.log('logout');
      this.authservice.SignOut().then();
    });
   }
   /*
   Si resetPassword
   */
   if ( mode === 'resetPassword') {
     // TODO gérer ca
    this.router.navigate(['/reset'], {queryParamsHandling: 'merge' });
   }

   });



  }

}
