import { Injectable, NgZone } from '@angular/core';
import { User } from '../interfaces/user';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import {from, Observable, of} from 'rxjs';
import { switchMap, filter, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { NotifierService } from 'angular-notifier';

import { auth } from 'firebase/app';


@Injectable({
  providedIn: 'root'
})

export class AuthService {

  userData: any; // Save logged in user data
  user: Observable<User>;
  notifier: NotifierService;
  constructor(
    public afs: AngularFirestore,   // Inject Firestore services
    public afAuth: AngularFireAuth, // Inject Firebase auth services
    public router: Router,
    private http: HttpClient,
    notifierService: NotifierService,
    public ngZone: NgZone // NgZone services to remove outside scope warning
  ) {
    this.notifier = notifierService;

    /* Saving user data in localstorage when
    logged in and setting up null when logged out */
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.userData = user;
        localStorage.setItem('user', JSON.stringify(this.userData));
        JSON.parse(localStorage.getItem('user'));
      } else {
        localStorage.setItem('user', null);
        JSON.parse(localStorage.getItem('user'));
      }
    });
    // Define the user observable
    this.user = this.afAuth.authState
      .pipe(
        switchMap(user => {

        if (user) {
          // logged in, get custom user from Firestore
          return this.afs.doc<User>(`users/${user.uid}`).valueChanges();
        } else {
          // logged out, null
          return of(null);
        }
      }),

      filter( x => x !== null),
      tap(() =>  console.log(this.afAuth.auth.currentUser.emailVerified)),

      );
  }


  // Returns true when user is looged in and email is verified

  // Sign in with email/password
  async SignIn(email, password) {
    localStorage.removeItem('user');
    const result = await this.afAuth.auth.signInWithEmailAndPassword(email, password)
      .catch((error) => {
        console.log(error);
        this.notifier.notify('error', error.message);
        throw Error('error.message');
      });

      this.SetUserData(result.user);
      localStorage.setItem('user', JSON.stringify(result.user));
      this.router.navigate(['/dashboard']);
  }




  // Sign up with email/password
  SignUp(email, password, sponsor) {
     return  from(this.afAuth.auth.createUserWithEmailAndPassword(email, password))
       .pipe(
      switchMap((result ) =>  this.SetUserData(result.user)),
      switchMap(( ) =>  this.SendVerificationMail())
     ).toPromise()
      .catch((error) => {
        console.log(error);
          const errorCode = error.code;
          // cas si mot de passe faible
          if (errorCode === 'auth/weak-password') {
            this.notifier.notify('info', 'The password is too weak.');
          } else if (errorCode === 'auth/email-already-in-use') {
            this.notifier.notify('error', 'The email is already use, try to reset your password');
          } else {
            this.notifier.notify('error', error.error.message);
          }
        });
      }




  // Send email verfificaiton when new user sign up
  SendVerificationMail() {
    return this.afAuth.auth.currentUser.sendEmailVerification()
      .then(() => {
        this.router.navigate(['/verify-email-address']);
      });
  }
 // Send email verfificaiton when new user sign up
 resendVerificationEmail() {

  return this.afAuth.auth.currentUser.sendEmailVerification()
    .then(() => {
      console.log('retour login ');
      this.router.navigate(['/sign-in']);
    })
    .catch((err) => console.log(err));
}


  // Reset Forggot password
  ForgotPassword(passwordResetEmail) {
    return this.afAuth.auth.sendPasswordResetEmail(passwordResetEmail)
      .then(() => {
        this.notifier.notify('info', 'Password reset email sent, check your inbox.');
      }).catch((error) => {
       this.notifier.notify('error', error);
      });
  }
get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return (user !== null && user.emailVerified !== false) ? true : false;
  }



  /* Setting up user data when sign in with username/password,
  sign up with username/password and sign in with social auth
  provider in Firestore database using AngularFirestore + AngularFirestoreDocument services */
  SetUserData(user) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
    const userData: User = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL,
      emailVerified: user.emailVerified,
    };

    return userRef.set(userData, {
      merge: true
    });
  }

  // Sign out
  SignOut(redirect = true) {
    return this.afAuth.auth.signOut().then(() => {
      localStorage.removeItem('user');
      if (redirect) {
        this.router.navigate(['/sign-in']);
      }
    });
  }


   // If error, console log and notify user
   private handleError(error: any) {
    this.notifier.notify('error', error.message);
  }

  public validateEmail(actionCode ) {
    return  this.afAuth.auth.applyActionCode(actionCode).then(function(resp) {
    // Email address has been verified.
      console.log(resp);
    // TODO: Display a confirmation message to the user.
    // You could also provide the user with a link back to the app.

    // TODO: If a continue URL is available, display a button which on
    // click redirects the user back to the app via continueUrl with
    // additional state determined from that URL's parameters.
  }).catch(function(error) {
    console.log(error);
    // Code is invalid or expired. Ask the user to verify their email address
    // again.
  });
  }

  resetPassword(code, newPassword: any) {
    return this.afAuth.auth.confirmPasswordReset(code, newPassword).then((resp) =>  {
      // Email address has been verified.
        // console.log(resp);
        this.router.navigate(['/sign-in']);
      // TODO: Display a confirmation message to the user.
      // You could also provide the user with a link back to the app.

      // TODO: If a continue URL is available, display a button which on
      // click redirects the user back to the app via continueUrl with
      // additional state determined from that URL's parameters.
    }).catch((error) => {
      console.log(error);
      this.handleError(error);
      // Code is invalid or expired. Ask the user to verify their email address
      // again.
    });
  }

  async googleSignin() {
    const provider = new auth.GoogleAuthProvider();
    const credential = await this.afAuth.auth.signInWithPopup(provider);
    await this.SetUserData(credential.user);
    this.router.navigate(['/dashboard']);
  }
}
