import { Component, OnInit } from '@angular/core';
import {ClientService} from '../../services/client.service';
import { Client } from 'src/app/shared/interfaces/client';
import {Observable} from 'rxjs';
import {ClientState} from '../../../shared/enums/client-state.enum';
import {Router} from '@angular/router';

@Component({
  selector: 'app-list-clients',
  templateUrl: './list-clients.component.html',
  styleUrls: ['./list-clients.component.css']
})
export class ListClientsComponent implements OnInit {
  clientHeaders = [ 'Name'  , 'Email', 'State', 'Actions'];
  clientsStates = Object.values(ClientState);


  public clients$: Observable<Client[]>;

  constructor(
    private clientService: ClientService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.clients$ = this.clientService.list();
  }

  async goToClient() {
    await this.router.navigate(['/clients', 'add']);
  }

  async update(event, client: Client) {
    await this.clientService.update(client).toPromise();

  }
}
